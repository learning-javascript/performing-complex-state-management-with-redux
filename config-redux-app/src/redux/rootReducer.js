import combineReducers from "redux/es/combineReducers";
import posts from './reducers/posts';

const rootReducer = combineReducers({
  posts: posts,
});

export default rootReducer;
